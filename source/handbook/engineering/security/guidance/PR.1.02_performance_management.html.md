---
layout: markdown_page
title: "PR.1.02 - Performance Management Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# PR.1.02 - Performance Management

## Control Statement

GitLab has established a check-in performance management process for on-going dialogue between managers and their reports. quarterly reminders are sent to managers to perform their regular check-in conversation.

## Context

The purpose of this control is to ensure managers and their direct reports are in ongoing, open conversation with one another to stay current with projects, tasks, roadblocks, and so on. This benefits both parties - particularly with GitLab being all-remote and asynchronous - by facilitating regular feedback, timely issue escalation, decision making, and work prioritization.

## Scope

This control applies to GitLab management and leadership.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/PR.1.02_performance_management.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/PR.1.02_performance_management.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/PR.1.02_performance_management.md).

## Framework Mapping

* SOC2 CC
  * CC1.3
